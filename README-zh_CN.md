## noVNC：HTML VNC客户端库和应用程序

[![测试状态](https://github.com/novnc/noVNC/workflows/Test/badge.svg)](https://github.com/novnc/noVNC/actions?query=workflow%3ATest) 
[![Lint状态](https://github.com/novnc/noVNC/workflows/Lint/badge.svg)](https://github.com/novnc/noVNC/actions?query=workflow%3ALint) 

### 描述

noVNC 是一个 HTML VNC 客户端 JavaScript 库，以及构建在该库之上的应用程序。noVNC 在任何现代浏览器中运行良好，包括移动浏览器（iOS 和 Android）。

许多公司、项目和产品已经集成了 noVNC，包括 [OpenStack](http://www.openstack.org)、[OpenNebula](http://opennebula.org/)、[LibVNCServer](http://libvncserver.sourceforge.net) 和 [ThinLinc](https://cendio.com/thinlinc)。更多详细信息和链接，请参见 [项目和公司维基页面](https://github.com/novnc/noVNC/wiki/Projects-and-companies-using-noVNC)。

### 目录

- [新闻/帮助/联系方式](#新闻帮助联系方式)
- [特性](#特性)
- [截图](#截图)
- [浏览器要求](#浏览器要求)
- [服务器要求](#服务器要求)
- [快速开始](#快速开始)
- [从Snap包安装](#从Snap包安装)
- [集成和部署](#集成和部署)
- [作者/贡献者](#作者贡献者)

### 新闻/帮助/联系方式

项目网站位于 [novnc.com](http://novnc.com)。

如果你是 noVNC 开发者/集成者/用户（或想成为），欢迎加入 [noVNC 讨论组](https://groups.google.com/forum/?fromgroups#!forum/novnc)。

错误和功能请求可以通过 [GitHub问题](https://github.com/novnc/noVNC/issues) 提交。如果你有关于使用 noVNC 的问题，请首先使用 [讨论组](https://groups.google.com/forum/?fromgroups#!forum/novnc)。我们还有一个 [维基](https://github.com/novnc/noVNC/wiki/)，包含许多有用的信息。

如果你想开始为 noVNC 做出贡献，一个好地方是标记为 ["patchwelcome"](https://github.com/novnc/noVNC/issues?labels=patchwelcome) 的问题。请查看我们的 [贡献指南](https://github.com/novnc/noVNC/wiki/Contributing)。

如果你想对 noVNC 表示赞赏，你可以向一些伟大的非营利组织捐款，例如：
[Compassion International](http://www.compassion.com/)、[SIL](http://www.sil.org)、[Habitat for Humanity](http://www.habitat.org)、[Electronic Frontier Foundation](https://www.eff.org/)、[Against Malaria Foundation](http://www.againstmalaria.com/)、[Nothing But Nets](http://www.nothingbutnets.net/) 等。

### 特性

* 支持所有现代浏览器，包括移动设备（iOS、Android）
* 支持的认证方法：无、经典 VNC、RealVNC 的 RSA-AES、Tight、VeNCrypt Plain、XVP、Apple 的 Diffie-Hellman、UltraVNC 的 MSLogonII
* 支持的 VNC 编码：raw、copyrect、rre、hextile、tight、tightPNG、ZRLE、JPEG、Zlib
* 支持缩放、剪辑和调整桌面大小
* 本地光标渲染
* 剪贴板复制/粘贴，支持完整的 Unicode
* 翻译
* 触摸手势，模拟常见鼠标动作
* 主要基于 [MPL 2.0](http://www.mozilla.org/MPL/2.0/) 许可，详见 [许可文件](LICENSE.txt)

### 截图

在 Firefox 中运行，连接前后：

![img](http://novnc.com/img/noVNC-1-login.png){: width="400px"}
![img](http://novnc.com/img/noVNC-3-connected.png){: width="400px"}

查看更多截图 [这里](http://novnc.com/screenshots.html)。

### 浏览器要求

noVNC 使用了许多现代网络技术，因此没有正式的要求列表。但是，这些是我们目前知道的最低版本：

* Chrome 64、Firefox 79、Safari 13.4、Opera 51、Edge 79

### 服务器要求

noVNC 遵循标准 VNC 协议，但与其他 VNC 客户端不同，它需要 WebSockets 支持。许多服务器包括支持（例如 [x11vnc/libvncserver](http://libvncserver.sourceforge.net/)、[QEMU](http://www.qemu.org/) 和 [MobileVNC](http://www.smartlab.at/mobilevnc/)），但对于其他服务器，你需要使用 WebSockets 到 TCP 套接字代理。noVNC 有一个姐妹项目 [websockify](https://github.com/novnc/websockify)，提供了一个简单的此类代理。

### 快速开始

* 使用 `novnc_proxy` 脚本自动下载并启动 websockify，它包括一个微型网络服务器和 WebSockets 代理。`--vnc` 选项用于指定正在运行的 VNC 服务器的位置：

    `./utils/novnc_proxy --vnc localhost:5901`
    
* 如果你不需要将网络服务器公开到互联网，你可以绑定到 localhost：

    `./utils/novnc_proxy --vnc localhost:5901 --listen localhost:6081`

* 将浏览器指向 `novnc_proxy` 脚本输出的剪切粘贴 URL。点击连接按钮，如果 VNC 服务器配置了密码，请输入密码，享受吧！

### 从Snap包安装
运行以下命令将从 Snap 安装 noVNC 的最新版本：

`sudo snap install novnc`

#### 直接从Snap运行noVNC

你可以使用以下命令直接运行从 Snap 包安装的 novnc，例如：

`novnc --listen 6081 --vnc localhost:5901 # /snap/bin/novnc 如果 /snap/bin 不在你的 PATH`

如果你想使用证书文件，由于标准的 Snap 限制，你需要将它们放在 /home/\<user\>/snap/novnc/current/ 目录中。如果你的用户名是 jsmith，一个示例命令将是：

  `novnc --listen 8443 --cert ~jsmith/snap/novnc/current/self.crt --key ~jsmith/snap/novnc/current/self.key --vnc ubuntu.example.com:5901`

#### 将noVNC作为服务（守护进程）从Snap运行
Snap 包还可以运行一个 'novnc' 服务，可以配置为监听多个端口连接到多个 VNC 服务器（实际上是运行多个 novnc 实例的服务）。
示例指令（带示例值）：

列出当前服务（开箱即用时这将是空白）：

```
sudo snap get novnc services
Key             Value
services.n6080  {...}
services.n6081  {...}
```

创建一个新服务，监听端口 6082 并连接到在 localhost 上运行的 VNC 服务器，端口为 5902：

`sudo snap set novnc services.n6082.listen=6082 services.n6082.vnc=localhost:5902`

（使用 'snap set' 定义的任何服务都将自动启动）
请注意，服务的名称，本例中的 'n6082'，可以是任何名称，只要它不以数字开头或包含空格/特殊字符。

查看刚刚创建的服务的配置：

```
sudo snap get novnc services.n6082
Key                    Value
services.n6082.listen  6082
services.n6082.vnc     localhost:5902
```

禁用服务（由于 Snap 的限制，目前无法取消设置配置变量，将它们设置为空值是禁用服务的方法）：

`sudo snap set novnc services.n6082.listen='' services.n6082.vnc=''`

（使用 'snap set' 这样设置为空的任何服务都将自动停止）

验证服务是否已禁用（空值）：

```
sudo snap get novnc services.n6082
Key                    Value
services.n6082.listen  
services.n6082.vnc
```

### 集成和部署

请参阅我们的其他文档，了解如何在你自己的软件中集成 noVNC，或在生产环境中部署 noVNC 应用程序：

* [嵌入](docs/EMBEDDING.md) - 对于 noVNC 应用程序
* [库](docs/LIBRARY.md) - 对于 noVNC JavaScript 库

### 作者/贡献者

请参阅 [AUTHORS](AUTHORS) 以获取（较为完整的）作者列表。如果你不在该列表上，但你认为你应该在，请随时发送 PR 进行修复。

* 核心团队：
    * [Samuel Mannehed](https://github.com/samhed)  (Cendio)
    * [Pierre Ossman](https://github.com/CendioOssman)  (Cendio)

* 以前的核心技术贡献者：
    * [Joel Martin](https://github.com/kanaka)  （项目创始人）
    * [Solly Ross](https://github.com/DirectXMan12)  (Red Hat / OpenStack)

* 显著贡献：
    * UI 和图标：Pierre Ossman, Chris Gordon
    * 原始徽标：Michael Sersen
    * tight 编码：Michael Tinglof (Mercuri.ca)
    * RealVNC RSA AES 认证：USTC Vlab 团队

* 包含的库：
    * base64：Martijn Pieters (Digital Creations 2), Samuel Sieb (sieb.net)
    * DES：Dave Zimmerman (Widget Workshop), Jef Poskanzer (ACME Labs)
    * Pako：Vitaly Puzrin (https://github.com/nodeca/pako) 

你想在这个名单上吗？查看我们的
[贡献指南](https://github.com/novnc/noVNC/wiki/Contributing)  并开始编码！